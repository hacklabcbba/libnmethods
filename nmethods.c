#include "nmethods.h"
#include <math.h>

/*************************************************************************************************/

#define feval(func, arg, a, b) ((((func)(a, arg)) - b))
#define ceval1(a, tol) ((fabs(a) < tol))
#define ceval2(a, b, tol) ((fabs(b - a) < tol))
#define ceval3(a, b, c) ((a * b < c))

/*************************************************************************************************/

void nmeth_bisect(double (*func)(double, void *), void *arg, double c, double xa, double xb, context_t *context)
{
	double ya = feval(func, arg, xa, c);
	double yb = feval(func, arg, xb, c);
	double tol = context->tol;
	uint32_t imax = context->imax;
	uint32_t icnt = 0;
	uint32_t cflag = 0;
	uint32_t eflag = 0;
	double x = 0.0, y = 0.0;

	if(ceval1(ya, tol))
	{
		x = xa;
		y = ya;
		cflag = 1;
	}
	else if(ceval1(yb, tol))
	{
		x = xb;
		y = yb;
		cflag = 1;
	}
	else if(ceval3(ya, yb, 0.0))
	{
		for(icnt = 1; icnt < imax; icnt++)
		{
			x = xa + (xb - xa) / 2.0;
			y = feval(func, arg, x, c);

			uint32_t cnd1 = ceval1(y, tol);
			uint32_t cnd2 = ceval2(xa, xb, tol);
			cflag = cnd1 || cnd2;
			if(cflag)
			{
				break;
			}
			else if(ceval3(y, ya, 0.0))
			{
				xb = x;
				yb = y;
			}
			else
			{
				xa = x;
				ya = y;
			}
		}
	}
	else
	{
		eflag = 1;
	}
	context->icnt = icnt;
	context->cflag = cflag;
	context->eflag = eflag;
	context->x = x;
	context->y = y;
}

void nmeth_regfalsi(double (*func)(double, void *), void *arg, double c, double xa, double xb, context_t *context)
{
	double ya = feval(func, arg, xa, c);
	double yb = feval(func, arg, xb, c);
	double tol = context->tol;
	uint32_t imax = context->imax;
	uint32_t icnt = 0;
	uint32_t cflag = 0;
	uint32_t eflag = 0;
	double x = 0.0, y = 0.0;

	if(ceval1(ya, tol))
	{
		x = xa;
		y = ya;
		cflag = 1;
	}
	else if(ceval1(yb, tol))
	{
		x = xb;
		y = yb;
		cflag = 1;
	}
	else if(ceval3(ya, yb, 0.0))
	{
		for(icnt = 1; icnt < imax; icnt++)
		{
			x = (xa * yb - xb * ya) / (yb - ya);
			y = feval(func, arg, x, c);

			uint32_t cnd1 = ceval1(y, tol);
			uint32_t cnd2 = ceval2(xa, xb, tol);
			cflag = cnd1 || cnd2;
			if(cflag)
			{
				break;
			}
			else if(ceval3(y, ya, 0.0))
			{
				double rperf = fabs((x - xb) / (x - xa));
				if(rperf < 1.0)
					ya = (ya * yb) / (yb + y);
				xb = x;
				yb = y;
			}
			else
			{
				double rperf = fabs((x - xa) / (x - xb));
				if(rperf < 1.0)
					yb = (ya * yb) / (ya + y);
				xa = x;
				ya = y;
			}
		}
	}
	else
	{
		eflag = 1;
	}
	context->icnt = icnt;
	context->cflag = cflag;
	context->eflag = eflag;
	context->x = x;
	context->y = y;
}