#ifndef __NMETHODS_H
#define __NMETHODS_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*************************************************************************************************/

typedef struct
{
	double tol;
	uint32_t imax;
	uint32_t icnt;
	uint32_t cflag;
	uint32_t eflag;
	double x;
	double y;
} context_t;

/*************************************************************************************************/

void nmeth_bisect(double (*func)(double, void *), void *arg, double c, double xa, double xb, context_t *context);
void nmeth_regfalsi(double (*func)(double, void *), void *arg, double c, double xa, double xb, context_t *context);

/*************************************************************************************************/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __NMETHODS_H */